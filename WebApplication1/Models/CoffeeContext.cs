﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WebApplication1.Models
{
    public class CoffeeContext : DbContext
    {
        public DbSet<Coffee> Coffees { get; set; }
        public DbSet<CoffeeDetails> CoffeeDetailss { get; set; }
        public DbSet<Volume> Volumes { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
    }
}