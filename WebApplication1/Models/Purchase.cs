﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Purchase
    {
        public int Id { get; set; }
        public String Person { get; set; }
        public String Address { get; set; }
        public int CoffeeDetailsId { get; set; }
        public DateTime Date { get; set; }
    }
}