﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class CoffeeDetails
    {
        public int Id { get; set; }
        public int CoffeeId { get; set; }
        public int VolumeId { get; set; }
        public int SugarSpoons { get; set; }
        public bool WithMilk { get; set; }
        public bool WithCupCap { get; set; }
    }
}