namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebApplication1.Models;

    public sealed class Configuration : DbMigrationsConfiguration<WebApplication1.Models.CoffeeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "WebApplication1.Models.CoffeeContext";
        }

        protected override void Seed(WebApplication1.Models.CoffeeContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            if (!context.Coffees.Any())
            {
                context.Coffees.AddOrUpdate(new Coffee { Name = "Espresso", ImageFile = "espresso.jpg" });
                context.Coffees.AddOrUpdate(new Coffee { Name = "Latte", ImageFile = "latte.jpg" });
                context.Coffees.AddOrUpdate(new Coffee { Name = "Americano", ImageFile = "americano.png" });
            }

            if (!context.Volumes.Any())
            {
                context.Volumes.AddOrUpdate(new Volume { Name = "0.133 L" });
                context.Volumes.AddOrUpdate(new Volume { Name = "0.250 L" });
                context.Volumes.AddOrUpdate(new Volume { Name = "0.500 L" });
            }

            base.Seed(context);
        }
    }
}
