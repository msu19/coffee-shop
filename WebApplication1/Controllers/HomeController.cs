﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        CoffeeContext db = new CoffeeContext();

        public ActionResult Index()
        {
            IEnumerable<Coffee> coffees = db.Coffees;
            ViewBag.Coffees = coffees;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "This is a page about how cool coffee we can make!";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Page with Coffee shop contacts";
            return View();
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            if (id == 0)
                return HttpNotFound();
            IEnumerable<Volume> volumes = db.Volumes;
            ViewBag.Volumes = volumes;
            ViewBag.CoffeeId = id;
            return View();
        }

        [HttpPost]
        public ActionResult Details(CoffeeDetails details)
        {
            db.CoffeeDetailss.Add(details);
            db.SaveChanges();
            ViewBag.CoffeeDetailsId = details.Id;
            return View("Buy");
        }

        [HttpGet]
        public ActionResult Buy()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Buy(Purchase purchase)
        {
            purchase.Date = DateTime.UtcNow;
            db.Purchases.Add(purchase);
            db.SaveChanges();
            if (purchase.Person == null)
                purchase.Person = "Guest";
            ViewBag.Message = purchase.Person + ", thanks for your choice and good luck!";
            return View("Thanks", db.Purchases.Find(purchase.Id));
        }
    }
}